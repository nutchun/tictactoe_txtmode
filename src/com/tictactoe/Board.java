package com.tictactoe;

public class Board {
	private int[][] board;
	private int turn, full;

	public Board() {
		this.turn = 1;
		this.full = 0;
	}

	public void setTableSize(int size) {
		board = new int[size][size];
	}

	public void setValue(int i, int j, int val) {
		board[i][j] = val;
	}

	public void setTurn(int val) {
		turn = val;
	}

	public void setFull() {
		full++;
	}

	public int getTableSize() {
		return board.length;
	}

	public int getValue(int i, int j) {
		return board[i][j];
	}

	public int getTurn() {
		return turn;
	}

	public int getFull() {
		return full;
	}

	public int getWinner() {
		/*
		 * Check the values that arrange in all row, for horizontal, vertical, diagonal
		 * and otherwise case, respectively.
		 */

		int size = getTableSize();

		// horizontal and vertical
		for (int i = 0; i < size; i++) {
			int sumHoriz = 0, sumVert = 0;
			for (int j = 0; j < size; j++) {
				if (getValue(i, j) != getValue(i, 0)) {
//					sumHoriz += getValue(i, j);
					sumHoriz = 0;
					break;
				} else {
					sumHoriz += getValue(i, j);
				}
			}
			for (int j = 0; j < size; j++) {
				if (getValue(j, i) != getValue(0, i)) {
//					sumVert += getValue(j, i);
					sumVert = 0;
					break;
				} else {
					sumVert += getValue(j, i);
				}
			}
			if ((sumHoriz == size) || (sumVert == size)) {
				return 1;
			} else if ((sumHoriz == 2 * size) || (sumVert == 2 * size)) {
				return 2;
			}
		}

		// diagonal
		int sumDiagonal1 = 0, sumDiagonal2 = 0;
		for (int i = 0; i < size; i++) {
			if (getValue(i, i) != getValue(0, 0)) {
				sumDiagonal1 = 0;
				break;
			} else {
				sumDiagonal1 += getValue(i, i);
			}
		}
		for (int i = 0; i < size; i++) {
			if (getValue(size - i - 1, i) != getValue(size - 1, 0)) {
				sumDiagonal2 = 0;
				break;
			} else {
				sumDiagonal2 += getValue(size - i - 1, i);
			}
		}
		if ((sumDiagonal1 == size) || (sumDiagonal2 == size)) {
			return 1;
		} else if ((sumDiagonal1 == 2 * size) || (sumDiagonal2 == 2 * size)) {
			return 2;
		}

		// otherwise
		if (full == size * size) {
			return 3;
		}

		return 0;
	}
}
